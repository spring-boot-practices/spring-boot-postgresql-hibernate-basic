package com.springboot.basic.postgresqlbasic.repository;

import com.springboot.basic.postgresqlbasic.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User,Long> {

    List<User> findAll();
}
