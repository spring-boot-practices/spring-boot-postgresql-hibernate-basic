# spring-boot-postgresql-hibernate-basic

This is a basic project of Spring Boot + Postgresql. Implement a CRUD.

Software:
- Docker v2.4.0.0
- Java 1.8

Steps for run:
1. Run Docker
2. Go to folder /docker and run command "start postgresqlFlyway.bat" for up postgresql and create user table with flyway.
3. Run program en IDE Intellij
4. Go to http://localhost:8080/swagger-ui.html# for use CRUD users.


Other comments:
- Use "start kill-services.bat" for reset database in docker.